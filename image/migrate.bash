#!/usr/bin/env bash

#
# Wrapper script around hasura-cli to offer easy migration functionality
#
# References:
#   https://hasura.io/docs/1.0/graphql/core/migrations/index.html#
#

#schemas="public,lego,pools"
default_schemas="public"

create() {
  # Update schemas variable with environment specified value if it is available
  schemas="${default_schemas}"
  if [ -n "${HASURA_GRAPHQL_MIGRATIONS_SCHEMAS+x}" ]; then
    echo "Setting schemas to ${HASURA_GRAPHQL_MIGRATIONS_SCHEMAS}"
    schemas="${HASURA_GRAPHQL_MIGRATIONS_SCHEMAS}"
  fi

  #Rewrite schemas input string into --schema arguments for hasura-cli
  schema_args=""
  IFS=',' read -ra schema_arr <<< "${schemas}"
  for i in "${schema_arr[@]}"; do
    # shellcheck disable=SC2089
    schema_args="$schema_args --schema \"${i}\""
  done

  #Migrate database
  echo "Migrating database"
  echo "Including schemas: ${schema_args}"
  tag=$(date +"%d-%m-%y %T")
  #shellcheck disable=SC2086,SC2090
  response=$(hasura-cli migrate create "${tag}" --from-server --database-name default ${schema_args} 2>&1)
  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Failed to create database migration"
    echo "Response: ${response}"
    exit 1
  fi

  # Extract version from response
  version=""
  while IFS= read -r line; do
    parsed=$(echo "$line" | jq -r -e ".version")
    # shellcheck disable=SC2181
    if [ $? -eq 0 ]; then
        version=${parsed}
    fi
  done <<< "${response}"

  if [ "${version}" == "" ]; then
    echo "Failed to parse version from response"
    exit 1
  fi

  # Mark the database migration as applied on this server
  response=$(hasura-cli migrate apply --version "${version}" --skip-execution --database-name default "${schema_args}" 2>&1)
  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Failed to apply database migration"
    echo "Response: ${response}"
    exit 1
  fi

  #Migrate database data
  #https://www.postgresql.org/docs/9.1/app-pgdump.html
  #pg_dump --column-inserts --data-only --schema=kite kitedb > /backup/
  #pg_dump --column-inserts --schema-only --schema=kite kitedb
  #psql -d kitedb -f /backup/kitedb_data.sql

  echo "Database migration done"

  # Migrate metadata
  echo "Migrating metadata"
  response=$(hasura-cli metadata export 2>&1)
  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Failed to create metadata export"
    echo "Response: ${response}"
    exit 1
  fi
  echo "Metadata migration done"
}

status() {
  hasura-cli migrate status --database-name default
}

help() {
  echo "Usage: migrate <options>"
  echo ""
  echo "--create"
  echo "--status"
  echo "--help"
  echo ""
  echo "Supported environment variables:"
  echo "  HASURA_GRAPHQL_MIGRATIONS_SCHEMAS     comma separated list if schema name to include. defaults to \"public\""
  echo ""
}

main() {
    if [ $# -eq 0 ]; then
      echo "Error: no options provided"
      help
      exit 1
    fi

    #
    # Process script arguments and parse all optional flags
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        --create)
            create
            ;;
        --status)
            status
            ;;
        --help)
            help
            ;;
        *)
            echo "Error: invalid option"
            echo ""
            help
            exit 1
            ;;
    esac
    shift # past argument or value
    done
}

main "$@"; exit 0