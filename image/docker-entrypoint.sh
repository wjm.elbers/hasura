#!/bin/sh

set -e

log() {
    TIMESTAMP=$(date -u "+%Y-%m-%dT%H:%M:%S.000+0000")
    LOGKIND=$1
    MESSAGE=$2
    echo "{\"timestamp\":\"$TIMESTAMP\",\"level\":\"info\",\"type\":\"startup\",\"detail\":{\"kind\":\"$LOGKIND\",\"info\":\"$MESSAGE\"}}"
}


PROJECT_DIR="/project"
DEFAULT_MIGRATIONS_DIR="${PROJECT_DIR}/migrations"
DEFAULT_METADATA_DIR="${PROJECT_DIR}/metadata"
DEFAULT_PORT=8080

if [ "${HASURA_GRAPHQL_AUTH_HOOK}" != "" ]; then
  #Wait for shared certificates to be available if auth hook is configured
  while [ ! -f "/tls/shared/auth/server.crt" ]; do echo "Waiting /tls/shared/auth/server.crt to become available"; sleep "1"; done

  #Update certificates
  cp /tls/shared/auth/server.crt /usr/local/share/ca-certificates/server.crt
  update-ca-certificates
fi

#Wait for database to become available
# shellcheck disable=SC2259
while ! echo exit | nc -z "${DB_HOST}" "${DB_PORT}" </dev/null; do echo 'Waiting for database to become available'; sleep "1"; done

# Use 9691 port for running temporary instance.
# In case 9691 is occupied (according to docker networking), then this will fail.
# override with another port in that case
# TODO: Find a proper random port
if [ -z ${HASURA_GRAPHQL_MIGRATIONS_SERVER_PORT+x} ]; then
    log "migrations-startup" "migrations server port env var is not set, defaulting to $DEFAULT_PORT"
    HASURA_GRAPHQL_MIGRATIONS_SERVER_PORT=$DEFAULT_PORT
fi

if [ -z ${HASURA_GRAPHQL_MIGRATIONS_SERVER_TIMEOUT+x} ]; then
    log "migrations-startup" "server timeout is not set, defaulting to 30 seconds"
    HASURA_GRAPHQL_MIGRATIONS_SERVER_TIMEOUT=30
fi

# wait for a port to be ready
wait_for_port() {
    PORT=$1
    log "migrations-startup" "waiting $HASURA_GRAPHQL_MIGRATIONS_SERVER_TIMEOUT for $PORT to be ready"
    # shellcheck disable=SC2034
    for i in $(seq 1 $HASURA_GRAPHQL_MIGRATIONS_SERVER_TIMEOUT);
    do
        nc -z localhost "$PORT" > /dev/null 2>&1 && log "migrations-startup" "port $PORT is ready" && return
        sleep 1
    done
    log "migrations-startup" "failed waiting for $PORT, try increasing HASURA_GRAPHQL_MIGRATIONS_SERVER_TIMEOUT (default: 30)" && exit 1
}

log "migrations-startup" "starting graphql engine temporarily on port $HASURA_GRAPHQL_MIGRATIONS_SERVER_PORT"

# start graphql engine with metadata api enabled
graphql-engine serve --enabled-apis="metadata" \
               --server-port=${HASURA_GRAPHQL_MIGRATIONS_SERVER_PORT}  &
# store the pid to kill it later
PID=$!

# wait for port to be ready
wait_for_port $HASURA_GRAPHQL_MIGRATIONS_SERVER_PORT

# check if migration directory is set, default otherwise
if [ -z ${HASURA_GRAPHQL_MIGRATIONS_DIR+x} ]; then
    log "migrations-startup" "env var HASURA_GRAPHQL_MIGRATIONS_DIR is not set, defaulting to $DEFAULT_MIGRATIONS_DIR"
    HASURA_GRAPHQL_MIGRATIONS_DIR="$DEFAULT_MIGRATIONS_DIR"
fi

# check if metadata directory is set, default otherwise
if [ -z ${HASURA_GRAPHQL_METADATA_DIR+x} ]; then
    log "migrations-startup" "env var HASURA_GRAPHQL_METADATA_DIR is not set, defaulting to $DEFAULT_METADATA_DIR"
    HASURA_GRAPHQL_METADATA_DIR="$DEFAULT_METADATA_DIR"
fi

if [ ! -f "$PROJECT_DIR/config.yml" ]; then
  log "migrations-apply" "generating config file $PROJECT_DIR/config.yaml"
  cd $PROJECT_DIR
  echo "version: 3" > config.yaml
  echo "endpoint: http://localhost:$HASURA_GRAPHQL_MIGRATIONS_SERVER_PORT" >> config.yaml
fi

# apply metadata if the directory exist
if [ -d "$HASURA_GRAPHQL_METADATA_DIR" ]; then
    #https://www.cyberciti.biz/faq/linux-unix-shell-check-if-directory-empty/
    if [ "$(ls -A $HASURA_GRAPHQL_METADATA_DIR)" ]; then
        log "migrations-apply" "applying metadata from $HASURA_GRAPHQL_METADATA_DIR"
        cd "$PROJECT_DIR"
        echo "metadata_directory: metadata" >> config.yaml
        hasura-cli metadata apply 
    else
        log "migrations-apply" "directory $HASURA_GRAPHQL_METADATA_DIR is empty, skipping metadata"
    fi
else
    log "migrations-apply" "directory $HASURA_GRAPHQL_METADATA_DIR does not exist, skipping metadata"
fi

# apply migrations if the directory exist
if [ -d "$HASURA_GRAPHQL_MIGRATIONS_DIR" ]; then
    #https://www.cyberciti.biz/faq/linux-unix-shell-check-if-directory-empty/
    if [ "$(ls -A $HASURA_GRAPHQL_MIGRATIONS_DIR)" ]; then
        log "migrations-apply" "applying migrations from $HASURA_GRAPHQL_MIGRATIONS_DIR"
        cd "$PROJECT_DIR"
        hasura-cli migrate apply --all-databases
        log "migrations-apply" "reloading metadata"
        hasura-cli metadata reload
    else
        log "migrations-apply" "directory $HASURA_GRAPHQL_MIGRATIONS_DIR is empty, skipping migrations"
    fi
else
    log "migrations-apply" "directory $HASURA_GRAPHQL_MIGRATIONS_DIR does not exist, skipping migrations"
fi

# kill graphql engine that we started earlier
log "migrations-shutdown" "killing temporary server"
kill $PID

# pass control to CMD
log "migrations-shutdown" "graphql-engine will now start in normal mode"
exec "$@"
